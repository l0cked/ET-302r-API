#!/usr/bin/python
import Adafruit_CharLCD as LCD
import sys
text = ""
text2 = ""
if len(sys.argv) > 2:
    text = sys.argv[1]
    text2 = sys.argv[2]
elif len(sys.argv) > 1:
    text = sys.argv[1]
else:
    text = "empty string"
    text2 = "empty string"


# Raspberry Pi pin configuration:
lcd_rs        = 26  # Note this might need to be changed to 21 for older revision Pi's.
lcd_en        = 19
lcd_d4        = 13
lcd_d5        = 6
lcd_d6        = 5
lcd_d7        = 11
#lcd_backlight = 4

# Define LCD column and row size for 16x2 LCD.
lcd_columns = 16
lcd_rows    = 2

# Initialize the LCD using the pins above.
lcd = LCD.Adafruit_CharLCD(lcd_rs, lcd_en, lcd_d4, lcd_d5, lcd_d6, lcd_d7,lcd_columns, lcd_rows)  #, lcd_backlight)

##################################

lcd.clear()
lcd.message ( text + "\n" + text2 )
