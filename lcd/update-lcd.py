#!/usr/bin/python
import memcache
import Adafruit_CharLCD as LCD

prog = ["Basic","Waves","Strokes","Pins","Random","AudioSoft","AudioLoud","Train","TrainInc","TrainSig"]

# Raspberry Pi pin configuration:
lcd_rs        = 26  # Note this might need to be changed to 21 for older revision Pi's.
lcd_en        = 19
lcd_d4        = 13
lcd_d5        = 6
lcd_d6        = 5
lcd_d7        = 11
#lcd_backlight = 4

# Define LCD column and row size for 16x2 LCD.
lcd_columns = 16
lcd_rows    = 2

# Initialize the LCD using the pins above.
lcd = LCD.Adafruit_CharLCD(lcd_rs, lcd_en, lcd_d4, lcd_d5, lcd_d6, lcd_d7,lcd_columns, lcd_rows)  #, lcd_backlight)

##################################

mc = memcache.Client(['127.0.0.1:11211'], debug=0)
lcd.clear()
A = str ( mc.get("ChA") )
B = str ( mc.get("ChB") )
P = str ( mc.get("Prg") )
try:
    Y =mc.get("heartbeat")
except:
    Y = "0" 
if not Y:
    Y = "0"
text = "ChA:" + A + " ChB:" + B + "  HR:"  + "\nPrg:" + P + " " + prog[int(P)] + "  " + Y 
lcd.message ( text )
