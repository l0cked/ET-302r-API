#!/bin/bash
# creates necessary symlinks

ln -s ../ZapMe.py First.py 
ln -s ../ZapMe.py orgHi.py 
ln -s ../ZapMe.py orgSoon.py 
ln -s ../ZapMe.py pins.py 
ln -s ../ZapMe.py rand.py 
ln -s ../ZapMe.py strks.py 
ln -s ../ZapMe.py wvs.py 
ln -s ../ZapMe.py dog.py 
ln -s ../ZapMe.py aDwn.py 
ln -s ../ZapMe.py aUp.py 
ln -s ../ZapMe.py bDwn.py 
ln -s ../ZapMe.py bUp.py 
ln -s ../ZapMe.py evlPnsh.py 
ln -s ../ZapMe.py hiPnsh.py 
ln -s ../ZapMe.py medPnsh.py 
ln -s ../ZapMe.py paus.py 
ln -s ../ZapMe.py prgDwn.py 
ln -s ../ZapMe.py prgUp.py 
ln -s ../ZapMe.py pnsh.py 
ln -s ../ZapMe.py resum.py 
ln -s ../ZapMe.py halt.py 
ln -s ../ZapMe.py sftPnsh.py 
ln -s ../ZapMe.py heart.py 
