#!/usr/bin/env python
# -*- coding: cp1252 -*-
"""Controls a ET302r"""

import sys
import RPi.GPIO as GPIO
from random import randint
import memcache
from time import sleep
from time import time
import subprocess  # only needed, if you want to use a 16x2 LCD Display for display of Ch A, Ch B and the mode you are in

#import atexit
#atexit.register(GPIO.cleanup)

# first install python memcached plugin

mc = memcache.Client(['127.0.0.1:11211'], debug=0)

# Define, what functions to call on startup, according to scriptname
# This technique is used to be interoperable with pyLauncher (v 1.1.1)
action = sys.argv[0]
action = action.split("/")[-1].split(".")[0]  # getting the calling script name

# Define max values for ChA and ChB -> for chicken ;-)
maxA = 50
maxB = 65
B0 = 11 #  3 # 11    # pin11 controls button 0
B1 = 13 # 5 # 13    # pin13 controls button 1
B2 = 15 # 7 # 15    # pin15 controls button 2
B3 = 19 # 11 #19    # pin19 controls button 3

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BOARD)   # Numbers GPIOs by physical location

GPIO.setup(B0, GPIO.OUT)   # Set B0's PIN mode to output
GPIO.setup(B1, GPIO.OUT)   # Set B1's PIN mode to output
GPIO.setup(B2, GPIO.OUT)   # Set B2's PIN mode to output
GPIO.setup(B3, GPIO.OUT)   # Set B3's PIN mode to output



# Electricity off - LEDS/PINS off

GPIO.output(B0, GPIO.LOW)
GPIO.output(B1, GPIO.LOW)
GPIO.output(B2, GPIO.LOW)
GPIO.output(B3, GPIO.LOW)


# Initialize values to 0
ChA = 0
ChB = 0
Prg = 0


## Defining functions



def quickpress(button, t=0.25, w=1 ):  # t=how long it's pressed, w=how often, default 1
    """Quickly presses and releases a button"""
    x = 0  # counter
 
    while x < w:

        GPIO.output(button, GPIO.HIGH)  # Press button
        sleep(t)

        GPIO.output(button, GPIO.LOW)  # Release button
        sleep(t)
        x += 1

def press(button):
    """Press and hold a button on the remote"""
    # Electricity turned on at pin' + str(button)

    GPIO.output(button, GPIO.HIGH)  # Press button

def release(button):
    """Release a button on the remote"""
    # Turn electricity off on PIN

    GPIO.setmode(GPIO.BOARD)
    GPIO.output(button, GPIO.LOW) # Release button

def aUp(ChA,w=1):
    """Turn channel A up one or more times"""
    if ChA + w > maxA:
        print "ChA already at maximum level"
        destroy()
        exit()

    x = 0  #counter

    # raising Channel A
    while x < w:
        quickpress(B3)
        x += 1

    print "ChA raised from " + str(ChA) + " to " + str(ChA + w)
    ChA = ChA + w
    mc.set("ChA",ChA)
    getVals()
    subprocess.call(["python", "../lcd/clear-lcd.py"])
    return()
    
def bUp(ChB,w=1): # channel B up one or more times
    """Turn channel B up one or more times"""
    if ChB + w > maxB:
        print "ChB already at maximum level"
        destroy()
        exit()   # Do nothing

    press (B1)   # Choose channel B
    sleep(0.2)
    x = 0  # counter

    # raising channel B
    while x < w:
        quickpress(B3)
        x += 1

    print "ChB raised from " + str(ChB) + " to " + str(ChB + w)
    sleep(0.2)
    release (B1) # deactivate channel B 

    ChB = ChB + w
    mc.set("ChB",ChB)
    getVals()
    subprocess.call(["python", "../lcd/clear-lcd.py"])
    return ()

def aDown(ChA,w=1):
    """Turn channel A down one or more times"""
    if ChA - w < 0:
        print "ChA already at minimum level"
        destroy()
        exit()   # Do nothing

    x = 0  # counter

    # channel A
    while x < w:
        quickpress(B2)
        x += 1
    print "ChA lowered from " + str(ChA) + " to " + str(ChA - w)

    ChA = ChA - w
    mc.set("ChA",ChA)
    getVals()
    subprocess.call(["python", "../lcd/clear-lcd.py"])
    return()
    
def bDown(ChB,w=1):
    """Turn channel B down one or more times"""
    if ChB - w < 0:
        print "ChB already at minimum level"
        destroy()
        exit()   # Do nothing

    press (B1)   # activate channel B
    sleep(0.1)
    x = 0  # counter

    # Channel B
    while x < w:
        quickpress(B2)
        x += 1
    print "ChB lowered from " + str(ChB) + " to " + str(ChB - w)
    sleep(0.1)
    release (B1) # deactivate channel B

    ChB = ChB - w
    mc.set("ChB",ChB)
    getVals()
    subprocess.call(["python", "../lcd/clear-lcd.py"])
    return ()
    

def prgUp(Prg,w=1):
    """Change the programm to a higher one"""
    print "Programm " + str(w) + " up."
    press(B0)
    sleep(0.2)
    for i in range(0,w):
        quickpress(B3)
    sleep(0.2)
    release(B0)
    Prg = Prg + w
    sleep(0.2)
    # quickpress(B1)  # activate Programm 
    if Prg >= 10:
        mc.set("Prg",Prg-10)
    else:
        mc.set("Prg",Prg)

    getVals()
    subprocess.call(["python", "../lcd/clear-lcd.py"])
    return()

    
def prgDown(Prg,w=1):
    """Change the programm to a lower one"""
    print "Programm " + str(w) + " down."
    press(B0)
    sleep(0.2)
    for i in range(0,w):
        quickpress (B2)
    release(B0)
    Prg = Prg - w
    sleep(0.2)
    # quickpress(B1)  # Programm aktivieren
    if Prg < 0:
        mc.set("Prg",Prg+10)
    else:
        mc.set("Prg",Prg)
    getVals()
    subprocess.call(["python", "../lcd/clear-lcd.py"])
    return()
    return(Prg)

def setPrg(desired):
    actual = mc.get("Prg")
    """Set the programm to desired value"""
    if (actual != desired):
        if actual < desired:
            prgUp(actual,desired-actual)  # put to higher programm
            actual = desired
        if desired < actual:
            prgDown(actual,actual-desired)
            actual = desired
    mc.set("Prg",desired)
    subprocess.call(["python", "../lcd/clear-lcd.py"])

    
def initialize (a=10,b=10):
    """Initialization with reasonable values"""
    if mc.get("Init_has_run") == "1":
        print "Init already run"
        destroy ()
        exit("1")  # dirty exit
    mc.set("Init_has_run", "1")
    mc.set("ChA", 0)
    mc.set("ChB", 0)
    mc.set("Prg", 0)
    mc.set("dog_mode_stop",0)
    x = 0  #counter

    subprocess.call(["python", "../lcd/clear-lcd.py"])


    # Channel A up
    while x < a:
        quickpress(B3)  # raise strength
        x += 1
    ChA = x

    # Channel B up
    x = 0   # counter
    press (B1)   # for activating "channel B up" mode


    while x < b:
        quickpress(B3)  # raise strength
        x += 1
    ChB = x
    release (B1) # deactivating "channel B up" mode

    setMem (ChA,ChB, Prg)
    getVals()
    return(ChA, ChB)

def shutdown():
    release(B0)
    release(B1)
    release(B2)
    release(B3)
    press(B0)
    press(B1)
    sleep(2)
    release(B0)
    release(B1)
    
def destroy():
    GPIO.setmode(GPIO.BOARD)   # Numbers GPIOs by physical location
    B0 = 3    # pin11
    B1 = 5    # pin13
    B2 = 7    # pin15
    B3 = 11    # pin19
    GPIO.setwarnings(False)
    #GPIO.output(B0, GPIO.LOW)
    #GPIO.output(B1, GPIO.LOW)
    #GPIO.output(B2, GPIO.LOW)
    #GPIO.output(B3, GPIO.LOW)
    GPIO.cleanup()                     # Release resource

def rampUp (ChA, ChB):
    setPrg(randint(0,4))
    #if ChA < 45:

def setMem (A = mc.get("ChA"),B = mc.get("ChB"),P = mc.get("Prg")):
    mc.set("ChA", A)
    mc.set("ChB", B)
    mc.set("Prg", P)

def getVals ():
    print "Channel A: " +  str(mc.get("ChA"))
    print "Channel B: " +  str(mc.get("ChB"))
    print "Programm: "  +  str(mc.get("Prg"))

def pause ():
    quickpress(B0)

def resume ():
    quickpress(B1)

def set_punish (a=25,b=35):
    pause ()
    
    if int(ChA) < a:
        aUp(ChA,a-int(ChA))
    
    if int(ChB) < b:
        bUp(ChB,b-int(ChB))
    
    setPrg(7)
    mc.set("ChA",a)
    mc.set("ChB",b)

def set_punish_slow (a=25,b=35):
    pause ()
    print "ChA = " + str(ChA)

    if int(ChA) < a:
        aUp(ChA,a-int(ChA))

    if int(ChB) < b:
        bUp(ChB,b-int(ChB))
    
    setPrg(8)

def trainmode (): # Duration: 30 minutes
    """if heartbeat under 120 - punish """
    
    setPrg(9) # give sporty spice 1st signal
    subprocess.call(["python", "../lcd/update-lcd.py"])
    quickpress(B2)
    
    sleep(240)
    
    quickpress(B2)
    setPrg(6)

    set_punish(25,40) ### FIXME ###
    
    subprocess.call(["python", "../lcd/update-lcd.py"])
    
    starttime = int(time())
        
    while int(time()) < starttime + 1200 : # training
        Y = int(mc.get("heartbeat"))
        
        print Y
        if Y and Y >= 115 and Y < 120: 
            print ("Y an between")
            quickpress(B1)
            sleep (10) 
        elif Y and Y < 115:    
            print ("too low")
            press(B2)
            sleep(2)
            release(B2)
            sleep (10)
        else: # 
            sleep (2)
    
    print "stopping"   
    setPrg(6)
    set_punish(18,30) ### FIXME ###
    setPrg(4) # back to other
    sleep (3)
    quickpress(B0)
    subprocess.call(["python", "../lcd/update-lcd.py"])



def dogmode (): # Duration: 2 minutes
    """forces wearer on all fours"""
    
    setPrg(9) # give doggy 1st signal
    quickpress(B1)
    
    sleep(6)
    
    setPrg(6)
    set_punish(35,45) ### FIXME ###
    
    scold = 2
    time_to_get_back_on_all_4s = 3
    
    starttime = int(time())
        
    while int(time()) < starttime + 120 : # dogmode, until someone sets var to <> 0
        Y = mc.get("angle")
        
        print Y
        if Y and Y < 2.0: #get the puppy back on all fours by force
            
            if scold == 0:
                print "Bad Doogy, Punish!"
                quickpress(B1)
                scold += 1 # raise scold level
                sleep (time_to_get_back_on_all_4s)
            
            elif scold == 1:
                print "Bad Doogy, Punish!!"
                press(B1)
                sleep(3)
                release(B1)
                scold += 1 # raise scold level
                sleep (time_to_get_back_on_all_4s)
            
            elif scold == 2:
                print "Bad Doogy, Punish!!!"
                quickpress(B2)
                scold += 1 # raise scold level
                sleep (time_to_get_back_on_all_4s)
            
            elif scold == 3:
                print "Bad Doogy, Punish!!!!"
                quickpress(B3)
                scold += 1 # raise scold level
                sleep (time_to_get_back_on_all_4s)
            
            elif scold == 4:
                print "Bad Doogy, Punish!!!!!"
                press(B3)
                sleep(2)
                release(B3)    
                scold += 1 # raise scold level
                sleep (time_to_get_back_on_all_4s)
            
            elif scold >= 5:
                print "Bad Doogy, Punish!!!!!!"
                press(B3)
                sleep(5)
                release(B3)
                sleep (time_to_get_back_on_all_4s)
            
        else: # good puppy, is on all fours
            scold = 2
            sleep (1)
    
    print "stopping"   
    #setPrg(9)
    #quickpress(B3) # give doggy stop signal
    #sleep(6)
    setPrg(6) # back to other
    sleep (3)
    mc.set("dog_mode_stop",0) # reset stopper
    print "dog mode stopped"
    quickpress(B0)


def set_orgasm (a=30,b=35):
    setPrg(randint(1,2))
    pause ()
    print "ChA = " + str(ChA)
    
    if int(ChA) < a:
        aUp(ChA,a-int(ChA))
    
    if int(ChB) < b:
        bUp(ChB,b-int(ChB))
   
    resume ()



## Functions End



## Main programm starts here
if __name__ == '__main__':     # Program start from here
    


    try:
        ChA = mc.get("ChA")
        ChB = mc.get("ChB")
        P = mc.get("Prg")
        
        if action == "First" and mc.get("Init_has_run") != "1": # Initialize now
            initialize(randint(15,16), randint (15,16))

        elif action == "First" and mc.get("Init_has_run") == "1": # already initalized
            print "Init already run."
            getVals()

        elif action != "First" and mc.get("Init_has_run") != "1": # not yet initialized
            print 'Run "First" to initialize first!'

        else:  # initialized and happy to procede
        
            if action == "prgUp":
                prgUp(P,1)
            
            elif action == "prgDwn":
                prgDown(P,1)
            
            elif action == "aUp":
                if P not in [7,8,9]:
                    A = mc.get("ChA")
                    aUp(A,1)
                else:
                    print "you are in Punish mode, not possible to raise or lower strength!"
            

            elif action == "bUp":
                if P not in [7,8,9]:
                    B = mc.get("ChB")
                    bUp(B,1)
                else:
                    print "you are in Punish mode, not possible to raise or lower strength!"
            

            elif action == "aDwn":
                if P not in [7,8,9]:
                    A = mc.get("ChA")
                    aDown(A,1)
                else:
                    print "you are in Punish mode, not possible to raise or lower strength!"
            

            elif action == "bDwn":
                if P not in [7,8,9]:
                    B = mc.get("ChB")
                    bDown(B,1)
                else:
                    print "you are in Punish mode, not possible to raise or lower strength!"
            

            elif action == "halt":  # shut down ET302r, reset values in memory
                shutdown()
                destroy()
                mc.set("Init_has_run",0)
                setMem(0,0,0)
            
            elif action == "pnsh":   # punish
                set_punish()
                sleep(randint(3,10))
                quickpress(B1)
            
            elif action == "sftPnsh":   # soft Punish
                set_punish()
                # sleep(randint(3,10))
                press(B1)
                print "Zap!"
                sleep(1)
                release(B1)
            
            elif action == "medPnsh":   # medium punish
                set_punish()
                press(B2)
                print "Zaap!"
                sleep(1)
                release(B2)
            
            elif action == "hiPnsh":    # high punish
                set_punish()
                press(B3)
                print "Zaaap!"
                sleep(1)
                release(B3)
            
            elif action == "evlPnsh":   # evil Punish
                set_punish()
                press(B3)
                print "Zaaaaap!"
                sleep(5)
                release(B3)
            
            elif action == "dog":       # get on all fours / canine mode
                print "current prog: " + str(P)
                dogmode()
                
            elif action == "paus":      # pause
                if P not in [7,8,9]:
                    pause ()
                    subprocess.call(["python", "../lcd/show-on-lcd.py", "paused"])
                    exit(0)
                else:
                    print "you are in Punish/Signal mode, not possible to pause or resume!"

            elif action == "resum":     # resume
                if P not in [7,8,9]:
                    resume ()
                    subprocess.call(["python", "../lcd/show-on-lcd.py", "resume"])
                    sleep(3)
                else:
                    print "you are in Punish/Signal mode, not possible to pause or resume!"
            
            elif action == "wvs":       # waves mode
                setPrg(1)
            
            elif action == "strks":     # stroke mode
                setPrg(2)
            
            elif action == "rand":      # random mode
                setPrg(4)
            
            elif action == "pins":      # mode 4
                setPrg(3)
            
            elif action == "orgSoon":   # sets waves on high, makes you go "whooo"
                set_orgasm()
            
            elif action == "orgHi":   # sets waves on higher, makes you go "whoOO"
                set_orgasm(40,48)
                
            elif action == "heart":     # checks heart rate, kepps you busy
                trainmode()
                
        destroy()
        subprocess.call(["python", "../lcd/update-lcd.py"])
        exit(0)

    except KeyboardInterrupt:  # When 'Ctrl+C' is pressed, the child program destroy() will be  executed.
        shutdown()
        destroy()
        mc.set("Init_has_run",0)
        setMem(0,0,0)
        mc.set("dog_mode_stop",0)

