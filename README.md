# DISCLAIMER
## Make sure you fully understand the code before you use it.  
## Only play below the waist!
## I'm not responsible, if any body parts you still need get fried. ;-)

This software is intended to serve as an API and a smart phone remote control
for the ET-302r signal generator. 

For the full fun, you need several components:
A smart phone to control them all, a spare ET-302r remote control, a soldering
machine, and an optocoupler with four channels (because we have four buttons)
and a Raspberry Pi.
A breadboard, several resistors, and eight little cables.

Optionally, you can attach a heart rate monitor via Bluetooth and you can use
an old android phone and attach it on someone's lower back to track the angle
of someone's spine. (This is only needed for canine mode.)
An LCD is optional but fun, and you have visual feedback.

### Requirements:
First you've got to assemble the hardware.
Then you've got to update your Raspberry Pi.
This can take quite a while.

```apt-get update && apt-get upgrade -y```

### You need several modules for the software to run.
```bash
pip install Adafruit-CharLCD  # for LCD

# important
apt-get install memcached -y # MemCache Daemon has to run (important!)
apt-get install python-memcache -y
apt-get install libcache-memcached-perl -y

apt-get install bluez -y  # bluetooth stuff for heart-rate fun
apt-get install python-beautifulsoup python-lxml -y
apt-get install screen -y

```

Consider copying the rc.local file to /etc/ .
`cp ./rc.local /etc/ && chmod +x /etc/rc.local`

You can also run bash ./INSTALL.txt from the command line. 
