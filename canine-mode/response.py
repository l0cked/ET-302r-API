#!/usr/bin/env python
'''
Created on 24.08.2015

@author: mgr
'''

import socket
import sys

# Create a TCP/IP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# Bind the socket to the port
server_address = ('0.0.0.0', 50000)
print >>sys.stderr, 'starting up on %s port %s' % server_address
sock.bind(server_address)

# Listen for incoming connections
sock.listen(1)

while True:
    connection, client_address = sock.accept()
    
    try:
        while True:
            data = connection.recv(20)
            if data:
                print >>sys.stderr, 'Answering to the client'
                connection.sendall("$LBP_CONNECTED,ACK")
            else:
                # print >>sys.stderr, 'no more data from', client_address
                break
            
    finally:
        # Clean up the connection
        connection.close()

