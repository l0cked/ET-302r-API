#!/usr/bin/env python
import socket
import sys
# make sure you install beautifulsoup4 and lxml
from bs4 import BeautifulSoup
#import select
#import Queue

# first install python memcached plugin
import memcache
import time
mc = memcache.Client(['127.0.0.1:11211'], debug=0)

if len (sys.argv) < 2:
    print '''\nYou need a smart phone with "Sir Isaac's Apple" installed.\nInside the app, put to 2 or 5 Hz and enable server mode\n
    Use the orientation sensor. Range: 360 degrees.
    Put phone on lower back, display facing upward, bottom of phone in
    direction of feet, top of phone towards your head.
    '''
    print "Usage: " + sys.argv[0] + " IP-Address of smartphone running Isaacs Apple in Server mode"
    sys.exit(1)
else:
    ip = sys.argv[1]
# ip = sys.argv[1]

# Create a TCP/IP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# You need a smart phone with "Sir Isaac's Apple" installed.
# Inside the app, put to 2 or 5 Hz and enable server mode

# Connect the socket to the port where the server is listening

server_address = (ip, 49999)
print >>sys.stderr, 'connecting to %s port %s' % server_address
sock.connect(server_address)

try:
    
    # Send data
    message = '$LBP_CONNECT,50000,50001'
    print >>sys.stderr, 'sending "%s"' % message
    sock.sendall(message)

    data = sock.recv(24)

    print >>sys.stderr, 'received "%s"' % data
    print "'" + data.strip() + "'"
    # print data.strip().split(",")
    
    # UDP Receiving Values
    sockUDP = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sockUDP.bind(("0.0.0.0",50001))
    x = 0
    while True:
        data, addr = sockUDP.recvfrom(512) # buffer size is 1024 bytes
        soup = BeautifulSoup(data,"lxml") # pip install beautifulsoup4, lxml
        value = float(soup.y.getText().replace(",","."))  # string to float
        mc.set("angle",value,2)
        mc.set("time",int(time.time()))
        print value
    


finally:
    print >>sys.stderr, 'closing sockets'
    sock.close()
    sockUDP.close()

